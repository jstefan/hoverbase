﻿using UnityEngine;

namespace Hoverbase
{
    [RequireComponent(typeof(Rigidbody))]
    public class Projectile : MonoBehaviour
    {
        private Rigidbody rb;
        private Transform targetTransform;

        [Range(1f, 50f)]
        [SerializeField] private float guideSpeed = 2f;

        private void Awake()
        {
            Destroy(gameObject, 10f);
            rb = GetComponent<Rigidbody>();
        }

        private void Update()
        {
            Guide();
        }

        private void OnCollisionEnter(Collision collision)
        {
            Debug.Log("Kaboom");
            Destroy(gameObject);
        }

        private void Guide()
        {
            if (targetTransform == null) { return; }

            Quaternion targetDirection = Quaternion.LookRotation(targetTransform.position - transform.position);

            transform.rotation = Quaternion.Slerp(transform.rotation, targetDirection, Time.deltaTime * guideSpeed);

            rb.AddForce(transform.forward * 100f);
        }

        public void SetTargetTransform(Transform target)
        {
            targetTransform = target;
        }
    }
}