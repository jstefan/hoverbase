﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Hoverbase
{
    public class HUDController : MonoBehaviour
    {
        [SerializeField] Camera cam;

        [SerializeField] GameObject hudTargetPrefab;
        [SerializeField] RectTransform hudTargetParent;

        [Range(2f, 50f)]
        [SerializeField] float hudTargetFloatSpeed = 20f;

        private Dictionary<HUDTarget, RectTransform> hudTargets = new Dictionary<HUDTarget, RectTransform>();

        private void OnEnable()
        {
            UIEvent.OnHUDTargetEnable += UIEvent_OnHUDTargetEnable;
        }
        private void OnDisable()
        {
            UIEvent.OnHUDTargetEnable -= UIEvent_OnHUDTargetEnable;
        }

        private void UIEvent_OnHUDTargetEnable(HUDTarget hudTarget)
        {
            if (hudTarget != null && !hudTargets.ContainsKey(hudTarget))
            {
                if (hudTargetPrefab == null) { Debug.LogWarning("Hud Target Prefab is missing. Target creation terminated."); return; }

                Vector3 initialPosition = cam.WorldToScreenPoint(hudTarget.transform.position);
                initialPosition = ClampToScreen(initialPosition);

                RectTransform hudTargetUI = Instantiate(hudTargetPrefab, initialPosition, Quaternion.identity, hudTargetParent).GetComponent<RectTransform>();

                if (hudTarget.GetTargetIcon() != null)
                {
                    hudTargetUI.GetComponent<Image>().sprite = hudTarget.GetTargetIcon();
                }

                hudTargets.Add(hudTarget, hudTargetUI);
            }
        }

        void Update()
        {
            UpdateHUD();
        }

        private void UpdateHUD()
        {
            if (!cam) { return; }

            foreach (KeyValuePair<HUDTarget, RectTransform> kvp in hudTargets)
            {
                if (kvp.Value != null)
                {
                    kvp.Value.anchoredPosition = Vector2.Lerp(
                        kvp.Value.anchoredPosition, 
                        ClampToScreen(cam.WorldToScreenPoint(kvp.Key.transform.position), 5f, 5f, 5f, 5f),
                        Time.deltaTime * hudTargetFloatSpeed);
                }
            }
        }

        private Vector2 ClampToScreen(Vector3 pos, float paddingLeft = 0f, float paddingRight = 0f, float paddingTop = 0f, float paddingBottom = 0f)
        {
            Vector2 clampedVec = Vector2.zero;

            if (pos.z < 0) { pos = -pos; }

            clampedVec.x = Mathf.Clamp(pos.x, 0 + paddingLeft, Screen.width - paddingRight);
            clampedVec.y = Mathf.Clamp(pos.y, 0 + paddingBottom, Screen.height - paddingTop);

            return clampedVec;
        }
    }

}