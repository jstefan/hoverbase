﻿using UnityEngine;

namespace Hoverbase
{
    public class HUDTarget : MonoBehaviour
    {
        [SerializeField] Sprite targetIcon;

        private void Start()
        {
            UIEvent.HUDTargetEnable(this);
        }

        public Sprite GetTargetIcon() { return targetIcon; }
    }
}