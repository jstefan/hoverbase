﻿
namespace Hoverbase
{
    public class UIEvent
    {
        public delegate void HUDAction(HUDTarget hudTarget);

        public static event HUDAction OnHUDTargetEnable;

        public static void HUDTargetEnable(HUDTarget hudTarget)
        {
            if (OnHUDTargetEnable != null) { OnHUDTargetEnable(hudTarget); }
        }
    }
}