﻿using UnityEngine;

namespace Hoverbase
{
    public class HoverdinController : MonoBehaviour
    {
        [SerializeField] Rigidbody rb;

        [Range(1f, 100000f)]
        [SerializeField] private float verticalAcceleration = 1000f;
        [Range(1f, 10000f)]
        [SerializeField] private float horizontalAcceleration = 1000f;

        private float currentVerticalThrust;
        private float currentHorizontalThrust;

        private Transform targetInFocus;

        void Update()
        {
            ParseInput();
        }

        private void FixedUpdate()
        {
            ApplyVechileThrust();
        }

        private void ParseInput()
        {
            float verticalAxis = Input.GetAxis("Vertical");
            float horizontalAxis = Input.GetAxis("Horizontal");

            currentVerticalThrust = verticalAxis * verticalAcceleration;
            currentHorizontalThrust = horizontalAxis * horizontalAcceleration;

            if (Input.GetKeyDown(KeyCode.F))
            {
                Focus();
            }
        }

        private void ApplyVechileThrust()
        {
            if (currentVerticalThrust != 0)
            {
                rb.AddForce(rb.transform.forward * currentVerticalThrust);
            }
            if (currentHorizontalThrust != 0)
            {
                rb.AddRelativeTorque(Vector3.up * currentHorizontalThrust);
            }
        }

        private void Focus()
        {
            // TODO
        }
    }
}