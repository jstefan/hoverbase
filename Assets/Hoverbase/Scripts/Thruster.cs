﻿using UnityEngine;

namespace Hoverbase
{
    public class Thruster : MonoBehaviour
    {

        [Range(0f, 10f)]
        [SerializeField] private float minimumHeight;
        [Range(1f, 10000f)]
        [SerializeField] private float thrustForce = 1f;
        [SerializeField] LayerMask surfaceLayer;

        [SerializeField] Rigidbody rb;
        [SerializeField] Transform rayStart;
        [SerializeField] Transform forcePos;

        private void FixedUpdate()
        {
            if (rb == null) { return; }

            Ray ray = new Ray(rayStart.position, rayStart.up);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, minimumHeight, surfaceLayer))
            {
                rb.AddForceAtPosition(forcePos.up * thrustForce * (1.0f - (hit.distance / minimumHeight)), forcePos.position);
            }
            else
            {
                if (forcePos.position.y > rb.transform.position.y)
                {
                    rb.AddForceAtPosition(-forcePos.up * thrustForce, forcePos.position);
                }
                else
                {
                    rb.AddForceAtPosition(forcePos.up * thrustForce, forcePos.position);
                }
            }
        }
    }
}