﻿
namespace Hoverbase
{
    public class AIStateFactory
    {
        public AIState.SearchAndDestroy searchAndDestroy { get; protected set; }
        public AIState.Attack attack { get; protected set; }

        public AIStateFactory(AI context)
        {
            searchAndDestroy = new AIState.SearchAndDestroy();
            attack = new AIState.Attack();

            searchAndDestroy.SetContextVariables(this, context);
            attack.SetContextVariables(this, context);
        }
    }
}

