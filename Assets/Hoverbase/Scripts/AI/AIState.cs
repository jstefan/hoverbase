﻿
namespace Hoverbase
{
    abstract public class AIState
    {
        private AI context;
        public AIStateFactory factory;

        virtual public void OnEnter() { }
        virtual public void Update() { }

        public void SetContextVariables(AIStateFactory factory, AI context)
        {
            this.factory = factory;
            this.context = context;
        }

        public class Alive : AIState { }
        public class Destroyed : AIState { }

        public class SearchAndDestroy : Alive
        {
            public override void Update()
            {
                base.Update();
                context.SearchAndDestroy();
            }
        }
        public class Attack : Alive
        {
            public override void Update()
            {
                base.Update();
                context.Attack();
            }
        }
    }
}