﻿using UnityEngine;

namespace Hoverbase
{
    public class Turret : AI
    {
        [Range(1f, 10f)]
        [SerializeField] float turretRotationSpeed = 3f;
        [Range(1f, 150f)]
        [SerializeField] float turretTargetRotationSpeed = 20f;
        [SerializeField] LayerMask targetLayerMask;

        [SerializeField] Transform turretHead;
        [SerializeField] Transform firePos;

        [SerializeField] GameObject projectilePrefab;
        private float projectileSpeed = 60f;

        private const float TARGET_VICINITY_CHECK_COOLDOWN_MAX = 1f;
        private float currentTargetVicinityCheckCooldown;
        private float detectionDistance = 150f;

        private const float FIRE_COOLDOWN_MAX = 2f;
        private float currentFireCooldown;

        private Rigidbody targetRb;

        public override void SearchAndDestroy()
        {
            base.SearchAndDestroy();

            if (currentTargetVicinityCheckCooldown > 0) { currentTargetVicinityCheckCooldown -= Time.deltaTime; }
            else
            {
                currentTargetVicinityCheckCooldown = TARGET_VICINITY_CHECK_COOLDOWN_MAX;

                targetRb = VisibleTarget();
                if (targetRb != null)
                {
                    Debug.Log("Got it");
                    SetState(factory.attack);
                }
            }

            turretHead.Rotate(Vector3.up * Time.deltaTime * turretRotationSpeed);
        }

        public override void Attack()
        {
            base.Attack();

            if (targetRb == null) { SetState(factory.searchAndDestroy); }

            if ((turretHead.position - targetRb.transform.position).sqrMagnitude > detectionDistance * detectionDistance)
            {
                Debug.Log("Resetting");
                targetRb = null;
                SetState(factory.searchAndDestroy);
                return;
            }

            Vector3 interceptPoint = FirstOrderIntercept
            (
                firePos.position,
                Vector3.zero,
                projectileSpeed,
                targetRb.transform.position,
                targetRb.velocity
            );

            Quaternion targetDirection = Quaternion.LookRotation(interceptPoint - turretHead.position);

            turretHead.rotation = Quaternion.Slerp(turretHead.rotation, targetDirection, Time.deltaTime * turretTargetRotationSpeed);

            if (currentFireCooldown < 0)
            {
                currentFireCooldown = FIRE_COOLDOWN_MAX;
                Fire();
            }
            else { currentFireCooldown -= Time.deltaTime; }
        }

        private void Fire()
        {
            Rigidbody projectileRb = Instantiate(projectilePrefab, firePos.position, firePos.rotation).GetComponent<Rigidbody>();

            projectileRb.velocity = projectileRb.transform.forward * projectileSpeed;
            Projectile p = projectileRb.GetComponent<Projectile>();
            if (p)
            {
                p.SetTargetTransform(targetRb.transform);
            }
        }

        private Rigidbody VisibleTarget()
        {
            Collider[] targetColliders = Physics.OverlapSphere(turretHead.position, detectionDistance, targetLayerMask);
            if (targetColliders.Length > 0)
            {
                Collider targetCollider = targetColliders[Random.Range(0, targetColliders.Length)];
                if (Physics.Raycast(turretHead.position, (targetCollider.transform.position - turretHead.transform.position).normalized, detectionDistance, targetLayerMask))
                {
                    return targetCollider.GetComponentInParent<Rigidbody>();
                }
            }
            return null;
        }

        // The following 2 functions were taken from http://wiki.unity3d.com/index.php/Calculating_Lead_For_Projectiles
        public static Vector3 FirstOrderIntercept
        (
	        Vector3 shooterPosition,
	        Vector3 shooterVelocity,
	        float shotSpeed,
	        Vector3 targetPosition,
	        Vector3 targetVelocity
        )
        {
	        Vector3 targetRelativePosition = targetPosition - shooterPosition;
	        Vector3 targetRelativeVelocity = targetVelocity - shooterVelocity;
	        float t = FirstOrderInterceptTime
	        (
		        shotSpeed,
		        targetRelativePosition,
		        targetRelativeVelocity
	        );
	        return targetPosition + t*(targetRelativeVelocity);
        }
        public static float FirstOrderInterceptTime
        (
            float shotSpeed,
            Vector3 targetRelativePosition,
            Vector3 targetRelativeVelocity
)
        {
            float velocitySquared = targetRelativeVelocity.sqrMagnitude;
            if (velocitySquared < 0.001f)
                return 0f;

            float a = velocitySquared - shotSpeed * shotSpeed;

            //handle similar velocities
            if (Mathf.Abs(a) < 0.001f)
            {
                float t = -targetRelativePosition.sqrMagnitude /
                (
                    2f * Vector3.Dot
                    (
                        targetRelativeVelocity,
                        targetRelativePosition
                    )
                );
                return Mathf.Max(t, 0f); //don't shoot back in time
            }

            float b = 2f * Vector3.Dot(targetRelativeVelocity, targetRelativePosition);
            float c = targetRelativePosition.sqrMagnitude;
            float determinant = b * b - 4f * a * c;

            if (determinant > 0f)
            { //determinant > 0; two intercept paths (most common)
                float t1 = (-b + Mathf.Sqrt(determinant)) / (2f * a),
                        t2 = (-b - Mathf.Sqrt(determinant)) / (2f * a);
                if (t1 > 0f)
                {
                    if (t2 > 0f)
                        return Mathf.Min(t1, t2); //both are positive
                    else
                        return t1; //only t1 is positive
                }
                else
                    return Mathf.Max(t2, 0f); //don't shoot back in time
            }
            else if (determinant < 0f) //determinant < 0; no intercept path
                return 0f;
            else //determinant = 0; one intercept path, pretty much never happens
                return Mathf.Max(-b / (2f * a), 0f); //don't shoot back in time
        }
    }
}