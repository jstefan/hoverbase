﻿using UnityEngine;

namespace Hoverbase
{
    public class AI : MonoBehaviour
    {
        private AIState _state;
        private AIState State
        {
            get { return _state; }
            set { _state = value; _state.OnEnter(); }
        }
        protected AIStateFactory factory;

        private void Awake()
        {
            factory = new AIStateFactory(this);
            SetState(factory.searchAndDestroy);
        }

        private void Update()
        {
            State.Update();
        }

        public virtual void SearchAndDestroy() { }
        public virtual void Attack() { }

        protected void SetState(AIState newState)
        {
            State = newState;
        }
    }

}